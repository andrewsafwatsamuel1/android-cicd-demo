build:
  maxIssues: 0 # break the build if more than 100 issues are found.
  excludeCorrectable: true # exclude issues that's auto-corrected from count.
  # weight for every rule-set (if a rule/rule-set weighted to 2 it will be counted as if 2 issues were found).
  # for more info https://detekt.github.io/detekt/configurations.html#build-failure
  weights:
  # complexity: 2
  # LongParameterList: 1
  # style: 1
  # comments: 1

config:
  validation: true
  warningsAsErrors: false
  # when writing own rules with new properties, exclude the property path e.g.: 'my_rule_set,.*>.*>[my_property]'
  excludes: ''

processors:
  active: true
  exclude:
    - 'DetektProgressListener'
  # - 'KtFileCountProcessor'
  # - 'PackageCountProcessor'
  # - 'ClassCountProcessor'
  # - 'FunctionCountProcessor'
  # - 'PropertyCountProcessor'
  # - 'ProjectComplexityProcessor'
  # - 'ProjectCognitiveComplexityProcessor'
  # - 'ProjectLLOCProcessor'
  # - 'ProjectCLOCProcessor'
  # - 'ProjectLOCProcessor'
  # - 'ProjectSLOCProcessor'
  # - 'LicenseHeaderLoaderExtension'

console-reports:
  active: true
  exclude:
    - 'ProjectStatisticsReport'
    - 'ComplexityReport'
    - 'NotificationReport'
    #  - 'FindingsReport'
    - 'FileBasedFindingsReport'

output-reports:
  active: true
  exclude:
  # - 'TxtOutputReport'
  # - 'XmlOutputReport'
  # - 'HtmlOutputReport'

comments:
  active: true
  AbsentOrWrongFileLicense:
    active: false
  CommentOverPrivateFunction:
    active: false
  CommentOverPrivateProperty:
    active: false
  DeprecatedBlockTag:
    active: true
  EndOfSentenceFormat:
    active: false
    endOfSentenceFormat: ([.?!][ \t\n\r\f<])|([.?!]$)
  UndocumentedPublicClass:
    active: false
    searchInNestedClass: true
    searchInInnerClass: true
    searchInInnerObject: true
    searchInInnerInterface: true
  UndocumentedPublicFunction:
    active: false
  UndocumentedPublicProperty:
    active: false

complexity:
  active: true
  ComplexCondition:
    active: true
    threshold: 4
  ComplexInterface:
    active: false
    threshold: 10
    includeStaticDeclarations: false
  ComplexMethod:
    active: true
    threshold: 15
    ignoreSingleWhenExpression: true
    ignoreSimpleWhenEntries: false
    ignoreNestingFunctions: false
    nestingFunctions: [ ]
  LabeledExpression:
    active: true
    ignoredLabels: ""
  LargeClass:
    active: true
    threshold: 600
  LongMethod:
    active: true
    threshold: 60
  LongParameterList:
    active: true
    excludes: [ "**/test/**" ]
    functionThreshold: 6
    constructorThreshold: 10
    ignoreDefaultParameters: false
    ignoreDataClasses: true
  MethodOverloading:
    active: false
    threshold: 6
  NamedArguments:
    active: true
    threshold: 4
  NestedBlockDepth:
    active: true
    threshold: 4
  ReplaceSafeCallChainWithRun:
    active: false
  StringLiteralDuplication:
    active: true
    threshold: 4
    ignoreAnnotation: true
    excludeStringsWithLessThan5Characters: false
  TooManyFunctions:
    active: true
    thresholdInFiles: 20
    thresholdInClasses: 20
    thresholdInInterfaces: 20
    thresholdInObjects: 20
    thresholdInEnums: 11
    ignoreDeprecated: false
    ignorePrivate: true
    ignoreOverridden: true
    excludes: [ '**/test/**' ]

coroutines:
  active: true
  GlobalCoroutineUsage:
    active: false
  RedundantSuspendModifier:
    active: true
  SleepInsteadOfDelay:
    active: true
  SuspendFunWithFlowReturnType:
    active: true

empty-blocks:
  active: true
  EmptyCatchBlock:
    active: true
    allowedExceptionNameRegex: '_|(ignore|expected).*'
  EmptyClassBlock:
    active: true
  EmptyDefaultConstructor:
    active: true
  EmptyDoWhileBlock:
    active: true
  EmptyElseBlock:
    active: true
  EmptyFinallyBlock:
    active: true
  EmptyForBlock:
    active: true
  EmptyFunctionBlock:
    active: true
    ignoreOverridden: true
  EmptyIfBlock:
    active: true
  EmptyInitBlock:
    active: true
  EmptyKtFile:
    active: true
  EmptySecondaryConstructor:
    active: true
  EmptyTryBlock:
    active: true
  EmptyWhenBlock:
    active: true
  EmptyWhileBlock:
    active: true

exceptions:
  active: true
  ExceptionRaisedInUnexpectedLocation:
    active: true
    methodNames: [ 'toString', 'hashCode', 'equals', 'finalize' ]
  InstanceOfCheckForException:
    active: true
  NotImplementedDeclaration:
    active: false
  ObjectExtendsThrowable:
    active: false
  PrintStackTrace:
    active: false
  RethrowCaughtException:
    active: true
  ReturnFromFinally:
    active: true
    ignoreLabeled: false
  SwallowedException:
    active: false
    ignoredExceptionTypes: [ 'NumberFormatException', 'InterruptedException', 'ParseException', 'MalformedURLException' ]
    allowedExceptionNameRegex: '_|(ignore|expected).*'
  ThrowingExceptionFromFinally:
    active: true
  ThrowingExceptionInMain:
    active: false
  ThrowingExceptionsWithoutMessageOrCause:
    active: true
    exceptions: [ 'ArrayIndexOutOfBoundsException', 'Error', 'Exception', 'IllegalMonitorStateException', 'NullPointerException', 'IndexOutOfBoundsException', 'RuntimeException', 'Throwable' ]
  ThrowingNewInstanceOfSameException:
    active: true
  TooGenericExceptionCaught:
    active: false
    exceptionNames: [ 'ArrayIndexOutOfBoundsException', 'Error', 'Exception', 'IllegalMonitorStateException', 'NullPointerException', 'IndexOutOfBoundsException', 'RuntimeException', 'Throwable' ]
    allowedExceptionNameRegex: '_|(ignore|expected).*'
  TooGenericExceptionThrown:
    active: false
    exceptionNames: [ 'Error', 'Exception', 'Throwable', 'RuntimeException' ]

formatting:
  active: true
  android: true
  autoCorrect: true
  AnnotationOnSeparateLine:
    active: false
    autoCorrect: true
  AnnotationSpacing:
    active: false
    autoCorrect: true
  ArgumentListWrapping:
    active: false
    autoCorrect: true
    indentSize: 4
    maxLineLength: 120
  ChainWrapping:
    active: true
    autoCorrect: true
  CommentSpacing:
    active: true
    autoCorrect: true
  EnumEntryNameCase:
    active: true
    autoCorrect: true
  Filename:
    active: true
  FinalNewline:
    active: false
    autoCorrect: true
    insertFinalNewLine: true
  ImportOrdering:
    active: false
    autoCorrect: true
    layout: '*,java.**,javax.**,kotlin.**,^'
  Indentation:
    active: true
    autoCorrect: true
    indentSize: 4
    continuationIndentSize: 4
  MaximumLineLength:
    active: false
    maxLineLength: 120
    ignoreBackTickedIdentifier: false
  ModifierOrdering:
    active: true
    autoCorrect: true
  MultiLineIfElse:
    active: false
    autoCorrect: true
  NoBlankLineBeforeRbrace:
    active: true
    autoCorrect: true
  NoConsecutiveBlankLines:
    active: true
    autoCorrect: true
  NoEmptyClassBody:
    active: true
    autoCorrect: true
  NoEmptyFirstLineInMethodBlock:
    active: true
    autoCorrect: true
  NoLineBreakAfterElse:
    active: true
    autoCorrect: true
  NoLineBreakBeforeAssignment:
    active: true
    autoCorrect: true
  NoMultipleSpaces:
    active: true
    autoCorrect: true
  NoSemicolons:
    active: true
    autoCorrect: true
  NoTrailingSpaces:
    active: true
    autoCorrect: true
  NoUnitReturn:
    active: true
    autoCorrect: true
  NoUnusedImports:
    active: true
    autoCorrect: true
  NoWildcardImports:
    active: false
  PackageName:
    active: false
    autoCorrect: true
  ParameterListWrapping:
    active: true
    autoCorrect: true
    indentSize: 4
    maxLineLength: 120
  SpacingAroundAngleBrackets:
    active: true
    autoCorrect: true
  SpacingAroundColon:
    active: true
    autoCorrect: true
  SpacingAroundComma:
    active: true
    autoCorrect: true
  SpacingAroundCurly:
    active: true
    autoCorrect: true
  SpacingAroundDot:
    active: true
    autoCorrect: true
  SpacingAroundDoubleColon:
    active: true
    autoCorrect: true
  SpacingAroundKeyword:
    active: true
    autoCorrect: true
  SpacingAroundOperators:
    active: true
    autoCorrect: true
  SpacingAroundParens:
    active: true
    autoCorrect: true
  SpacingAroundRangeOperator:
    active: true
    autoCorrect: true
  SpacingAroundUnaryOperator:
    active: true
    autoCorrect: true
  SpacingBetweenDeclarationsWithAnnotations:
    active: true
    autoCorrect: true
  SpacingBetweenDeclarationsWithComments:
    active: false
    autoCorrect: true
  StringTemplate:
    active: true
    autoCorrect: true

naming:
  active: true
  ClassNaming:
    active: true
    classPattern: '[A-Z][a-zA-Z0-9]*'
  ConstructorParameterNaming:
    active: true
    parameterPattern: '[a-z][A-Za-z0-9]*'
    privateParameterPattern: '[a-z][A-Za-z0-9]*'
    excludeClassPattern: '$^'
    ignoreOverridden: true
  EnumNaming:
    active: true
    enumEntryPattern: '[A-Z][_a-zA-Z0-9]*'
  ForbiddenClassName:
    active: false
    forbiddenName: [ ]
  FunctionMaxLength:
    active: false
    maximumFunctionNameLength: 30
  FunctionMinLength:
    active: false
    minimumFunctionNameLength: 3
  FunctionNaming:
    active: true
    functionPattern: '([a-z][a-zA-Z0-9]*)|(`.*`)'
    excludeClassPattern: '$^'
    ignoreOverridden: true
    ignoreAnnotated: [ 'Composable','Test' ]
  FunctionParameterNaming:
    active: true
    parameterPattern: '[a-z][A-Za-z0-9]*'
    excludeClassPattern: '$^'
    ignoreOverridden: true
  InvalidPackageDeclaration:
    active: false
    rootPackage: ''
  MatchingDeclarationName:
    active: true
    mustBeFirst: true
  MemberNameEqualsClassName:
    active: true
    ignoreOverridden: true
  NoNameShadowing:
    active: true
  NonBooleanPropertyPrefixedWithIs:
    active: false
  ObjectPropertyNaming:
    active: true
    constantPattern: '[A-Za-z][_A-Za-z0-9]*'
    propertyPattern: '[A-Za-z][_A-Za-z0-9]*'
    privatePropertyPattern: '(_)?[A-Za-z][_A-Za-z0-9]*'
  PackageNaming:
    active: false
    packagePattern: '[a-z]+(\.[a-z][A-Za-z0-9]*)*'
  TopLevelPropertyNaming:
    active: true
    constantPattern: '[A-Z][_A-Z0-9]*'
    propertyPattern: '[A-Za-z][_A-Za-z0-9]*'
    privatePropertyPattern: '_?[A-Za-z][_A-Za-z0-9]*'
  VariableMaxLength:
    active: false
    maximumVariableNameLength: 64
  VariableMinLength:
    active: false
    minimumVariableNameLength: 1
  VariableNaming:
    active: true
    variablePattern: '[a-z][A-Za-z0-9]*'
    privateVariablePattern: '(_)?[a-z][A-Za-z0-9]*'
    excludeClassPattern: '$^'
    ignoreOverridden: true

performance:
  active: true
  ArrayPrimitive:
    active: true
  ForEachOnRange:
    active: true
  SpreadOperator:
    active: true
  UnnecessaryTemporaryInstantiation:
    active: true

potential-bugs:
  active: true
  CastToNullableType:
    active: false
  Deprecation:
    active: true
  DontDowncastCollectionTypes:
    active: false
  DoubleMutabilityForCollection:
    active: false
  DuplicateCaseInWhenExpression:
    active: true
  EqualsAlwaysReturnsTrueOrFalse:
    active: true
  EqualsWithHashCodeExist:
    active: true
  ExitOutsideMain:
    active: false
  ExplicitGarbageCollectionCall:
    active: true
  HasPlatformType:
    active: false
  IgnoredReturnValue:
    active: false
    restrictToAnnotatedMethods: true
    returnValueAnnotations: [ '*.CheckReturnValue', '*.CheckResult' ]
  ImplicitDefaultLocale:
    active: true
  ImplicitUnitReturnType:
    active: false
    allowExplicitReturnType: true
  InvalidRange:
    active: true
  IteratorHasNextCallsNextMethod:
    active: true
  IteratorNotThrowingNoSuchElementException:
    active: true
  LateinitUsage:
    active: false
    excludeAnnotatedProperties: [ ]
    ignoreOnClassesPattern: ''
  MapGetWithNotNullAssertionOperator:
    active: false
  MissingWhenCase:
    active: true
    allowElseExpression: true
  NullableToStringCall:
    active: false
  RedundantElseInWhen:
    active: true
  UnconditionalJumpStatementInLoop:
    active: true
  UnnecessaryNotNullOperator:
    active: true
  UnnecessarySafeCall:
    active: true
  UnreachableCatchBlock:
    active: true
  UnreachableCode:
    active: true
  UnsafeCallOnNullableType:
    active: true
  UnsafeCast:
    active: true
  UnusedUnaryOperator:
    active: true
  UselessPostfixExpression:
    active: false
  WrongEqualsTypeParameter:
    active: true

style:
  active: true
  ClassOrdering:
    active: false
  CollapsibleIfStatements:
    active: true
  DataClassContainsFunctions:
    active: false
    conversionFunctionPrefix: 'to'
  DataClassShouldBeImmutable:
    active: false
  DestructuringDeclarationWithTooManyEntries:
    active: false
    maxDestructuringEntries: 3
  EqualsNullCall:
    active: true
  EqualsOnSignatureLine:
    active: false
  ExplicitCollectionElementAccessMethod:
    active: false
  ExplicitItLambdaParameter:
    active: false
  ExpressionBodySyntax:
    active: false
    includeLineWrapping: false
  ForbiddenComment:
    active: true
    values: [ 'TODO:', 'FIXME:', 'STOPSHIP:' ]
    allowedPatterns: ''
  ForbiddenImport:
    active: false
    imports: [ ]
    forbiddenPatterns: ''
  ForbiddenMethodCall:
    active: false
    methods: [ 'kotlin.io.println', 'kotlin.io.print' ]
  ForbiddenPublicDataClass:
    active: false
    ignorePackages: [ '*.internal', '*.internal.*' ]
  ForbiddenVoid:
    active: false
    ignoreOverridden: false
    ignoreUsageInGenerics: false
  FunctionOnlyReturningConstant:
    active: true
    ignoreOverridableFunction: true
    ignoreActualFunction: true
    excludedFunctions: ''
    excludeAnnotatedFunction: [ 'dagger.Provides' ]
  LibraryCodeMustSpecifyReturnType:
    active: true
  LibraryEntitiesShouldNotBePublic:
    active: false
  LoopWithTooManyJumpStatements:
    active: true
    maxJumpCount: 1
  MagicNumber:
    active: true
    ignoreNumbers: [ '-1', '0', '1', '2' ]
    ignoreHashCodeFunction: true
    ignorePropertyDeclaration: false
    ignoreLocalVariableDeclaration: false
    ignoreConstantDeclaration: true
    ignoreCompanionObjectPropertyDeclaration: true
    ignoreAnnotation: false
    ignoreNamedArgument: true
    ignoreEnums: false
    ignoreRanges: false
    ignoreExtensionFunctions: true
  MandatoryBracesIfStatements:
    active: false
  MandatoryBracesLoops:
    active: false
  MaxLineLength:
    active: false
    maxLineLength: 120
    excludePackageStatements: true
    excludeImportStatements: true
    excludeCommentStatements: false
  MayBeConst:
    active: true
  ModifierOrder:
    active: true
  MultilineLambdaItParameter:
    active: false
  NestedClassesVisibility:
    active: true
  NewLineAtEndOfFile:
    active: false
  NoTabs:
    active: false
  ObjectLiteralToLambda:
    active: true
  OptionalAbstractKeyword:
    active: true
  OptionalUnit:
    active: false
  OptionalWhenBraces:
    active: true
  PreferToOverPairSyntax:
    active: true
  ProtectedMemberInFinalClass:
    active: true
  RedundantExplicitType:
    active: true
  RedundantHigherOrderMapUsage:
    active: true
  RedundantVisibilityModifierRule:
    active: true
  ReturnCount:
    active: true
    max: 2
    excludedFunctions: 'equals'
    excludeLabeled: false
    excludeReturnFromLambda: true
    excludeGuardClauses: false
  SafeCast:
    active: true
  SerialVersionUIDInSerializableClass:
    active: true
  SpacingBetweenPackageAndImports:
    active: true
  ThrowsCount:
    active: true
    max: 2
  TrailingWhitespace:
    active: false
  UnderscoresInNumericLiterals:
    active: false
    acceptableDecimalLength: 5
  UnnecessaryAbstractClass:
    active: true
    excludeAnnotatedClasses: [ 'dagger.Module' ]
  UnnecessaryAnnotationUseSiteTarget:
    active: false
  UnnecessaryApply:
    active: true
  UnnecessaryFilter:
    active: false
  UnnecessaryInheritance:
    active: true
  UnnecessaryLet:
    active: false
  UnnecessaryParentheses:
    active: false
  UntilInsteadOfRangeTo:
    active: false
  UnusedImports:
    active: false
  UnusedPrivateClass:
    active: true
  UnusedPrivateMember:
    active: true
    allowedNames: '(_|ignored|expected|serialVersionUID)'
  UseArrayLiteralsInAnnotations:
    active: false
  UseCheckNotNull:
    active: true
  UseCheckOrError:
    active: true
  UseDataClass:
    active: false
    excludeAnnotatedClasses: [ ]
    allowVars: false
  UseEmptyCounterpart:
    active: true
  UseIfEmptyOrIfBlank:
    active: true
  UseIfInsteadOfWhen:
    active: true
  UseIsNullOrEmpty:
    active: true
  UseOrEmpty:
    active: true
  UseRequire:
    active: true
  UseRequireNotNull:
    active: true
  UselessCallOnNotNull:
    active: true
  UtilityClassWithPublicConstructor:
    active: true
  VarCouldBeVal:
    active: true
  WildcardImport:
    active: false
    excludeImports: [ 'java.util.*', 'kotlinx.android.synthetic.*' ]